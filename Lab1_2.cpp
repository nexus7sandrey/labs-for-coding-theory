﻿#pragma warning(disable : 4996)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_matrix(int** M, int N_y, int N_x)
{
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            printf("%d ", M[i][j]);
        }
        printf("\n");
    }
}

int** create_massive(int N_y, int N_x)
{
    int** M = (int**)calloc(N_y, sizeof(int*));
    for (int i = 0; i < N_y; i++)
    {
        M[i] = (int*)calloc(N_x, sizeof(int));
    }
    return M;
}

void free_matrix(int** M, int N_y)
{
    for (int i = 0; i < N_y; i++)
    {
        free(M[i]);
    }
    free(M);
}

//Делаем перестановку столбиков обратно
void change_stolbiki(int** M_, int N_y, int N_x, int* perestanovki, int base_start, int count_stolbikov)
{
    for (int k = count_stolbikov - 2; k >= 0; k -= 2)
    {
        //printf("Pererstavlaem %d %d\n", perestanovki[k], perestanovki[k + 1]);
        //if (base_start != 0)
        //{
            for (int i = 0; i < N_x - N_y; i++)
            {
                int tmp = M_[i][perestanovki[k]];
                M_[i][perestanovki[k]] = M_[i][perestanovki[k + 1]];
                M_[i][perestanovki[k + 1]] = tmp;
            }
        //}
        //else
        //{
        //    for (int i = N_x - N_y; i < N_x; i++)
        //    {
        //        int tmp = M_[i][perestanovki[k]];
        //        M_[i][perestanovki[k]] = M_[i][perestanovki[k + 1]];
        //        M_[i][perestanovki[k + 1]] = tmp;
        //    }
        //}
        //print_matrix(M_, N_x - N_y, N_x);
    }
}

//Проверяем нужны ли перестановки 
int check_perestanovka(int** M, int N_y, int N_x, int* perestanovka, int* base_start, int* base_finish)
{
    int* check = (int*)calloc(N_x, sizeof(int)); //сюда запишем позиции единичек в столбцах единичных матриц
    memset(check, -1, N_x * sizeof(int));
    for (int i = 0; i < N_x; i++)
    {
        int count_0 = 0;
        int count_1 = 0;
        int last_pos_1 = -1;
        for (int j = 0; j < N_y; j++)
        {
            if (M[j][i] == 0)
            {
                count_0++;
            }
            if (M[j][i] == 1)
            {
                count_1++;
                last_pos_1 = j;
            }
        }
        if ((count_0 == N_y - 1) && (count_1 == 1))
        {
            check[i] = last_pos_1;
        }
    }

    printf("Before perestanovki: \n");
    for (int i = 0; i < N_x; i++)
    {
        printf("%d ", check[i]);
    }
    printf("\n");

    int left_value = 0; //выбираем, слева или справа будем помещать единичную матрицу
    int right_value = 0;
    for (int i = 0; i < N_y; i++)
    {
        if (check[i] == i)
        {
            left_value++;
        }
    }
    for (int i = N_x - N_y; i < N_x; i++)
    {
        if (check[i] == i - (N_x - N_y))
        {
            right_value++;
        }
    }

    printf("Values of left = %d; right = %d\n", left_value, right_value);
    if (left_value > right_value)
    {
        (*base_start) = 0;
        (*base_finish) = N_y;
    }
    else
    {
        (*base_start) = N_x - N_y;
        (*base_finish) = N_x;
    }

    int count_stolbiki = 0; //делаем перестановки при необходимости в массиве для перестановок
    for (int i = (*base_start); i < (*base_finish); i++)
    {
        if (check[i] != i - (*base_start))
        {
            for (int j = 0; j < N_x; j++)
            {
                if (check[j] == i - (*base_start))
                {
                    printf("Change elements: %d and %d\n", i, j);
                    int tmp = check[j];
                    check[j] = check[i];
                    check[i] = tmp;
                    perestanovka[count_stolbiki] = i;
                    perestanovka[count_stolbiki + 1] = j;
                    count_stolbiki += 2;
                    break;
                }
            }
        }
    }

    printf("After perestanovki: \n");
    for (int i = 0; i < N_x; i++)
    {
        printf("%d ", check[i]);
    }
    printf("\n");

    if (count_stolbiki > 0) //теперь делаем перестановки в самой матрице
    {
        for (int k = 0; k < count_stolbiki; k += 2)
        {
            for (int i = 0; i < N_y; i++)
            {
                int tmp = M[i][perestanovka[k]];
                M[i][perestanovka[k]] = M[i][perestanovka[k + 1]];
                M[i][perestanovka[k + 1]] = tmp;
            }
        }
    }

    printf("Matrix in standart view\n");
    print_matrix(M, N_y, N_x);
    return count_stolbiki;
}

//Проверяем и делаем, чтобы все числа были по модулю q
int check_modul(int** M, int N_y, int N_x, int q)
{
    int check = 0;
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            if (M[i][j] < 0)
            {
                while (M[i][j] < 0)
                {
                    M[i][j] += q;
                }
                check++;
            }
            else if (M[i][j] >= q)
            {
                M[i][j] = M[i][j] % q;
                check++;
            }
        }
    }
    return check;
}

//Выполенение траспонирования матрицы 
void change_matrix(int** M, int** M_, int N_y, int N_x, int q, int base_start)
{
    if (base_start == 0) //единичная матрица слева
    {
        for (int i = 0; i < N_y; i++) //ставим матрицу A
        {
            for (int j = N_y; j < N_x; j++)
            {
                M_[j - N_y][i] = -1 * M[i][j];
            }
        }
        for (int i = N_y; i < N_x; i++) //ставим единички для E
        {
            M_[i - N_y][i] = 1;
        }
    }
    else
    {
        for (int i = 0; i < N_y; i++)
        {
            for (int j = 0; j < N_x - N_y; j++)
            {
                M_[j][i + N_x - N_y] = -1 * M[i][j];
            }
        }
        for (int i = 0; i < N_x - N_y; i++)
        {
            M_[i][i] = 1;
        }
    }

    printf("Transporting...\n");
    print_matrix(M_, N_x - N_y, N_x);
    check_modul(M_, N_x - N_y, N_x, q);
}

int open_files(FILE** input, FILE** output, char* argv_0)
{
    char* path_program = (char*)calloc(strlen(argv_0), sizeof(char));
    path_program = argv_0;
    for (int i = strlen(path_program); i > 0; i--)
    {
        if (path_program[i] == '\\')
        {
            path_program[i + 1] = '\0';
            break;
        }
    }

    char* input_file = (char*)calloc(strlen(path_program) + 9, sizeof(char));
    input_file = strcpy(input_file, path_program);
    input_file = strcat(input_file, "input.txt");

    char* output_file = (char*)calloc(strlen(path_program) + 10, sizeof(char));
    output_file = strcpy(output_file, path_program);
    output_file = strcat(output_file, "output.txt");

    (*input) = fopen(input_file, "r");
    (*output) = fopen(output_file, "w");
    int error = 0;
    if (input == NULL)
    {
        printf("Error in open input file!\n");
        error--;
    }
    if (output == NULL)
    {
        printf("Error in open output file!\n");
        error--;
    }

    //free(input_file);
    //free(output_file);
    return error;
}

int main(int argc, char* argv[])
{
    FILE* input = NULL;
    FILE* output = NULL;
    if (open_files(&input, &output, argv[0]) < 0)
    {
        return 0;
    }

    int q;
    int N_x;
    int N_y;

    fscanf(input, "%d", &q);
    fscanf(input, "%d", &N_y);
    fscanf(input, "%d", &N_x);

    printf("q = %d; N_y = %d; N_x = %d\n", q, N_y, N_x);

    char buffer;
    int** M = create_massive(N_y, N_x);
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            fscanf(input, "%d", &M[i][j]);
        }
    }

    printf("Scanned matrix (N_y = %d, N_x = %d): \n", N_y, N_x);
    print_matrix(M, N_y, N_x);

    if (check_modul(M, N_y, N_x, q) != 0)
    {
        printf("Matrix was edited:\n");
        print_matrix(M, N_y, N_x);
    }

    int* perestanovki = (int*)calloc(N_y * 2, sizeof(int)); //сюда запишем позиции единичек в столбцах единичных матриц
    memset(perestanovki, -1, N_x * sizeof(int));
    int base_start;
    int base_finish;
    int count_stolbikov = check_perestanovka(M, N_y, N_x, perestanovki, &base_start, &base_finish);

    int** M_ = create_massive(N_x - N_y, N_x);
    for (int i = 0; i < N_x - N_y; i++)
    {
        memset(M_[i], 0, N_x * sizeof(int));
    }
    change_matrix(M, M_, N_y, N_x, q, base_start);
    printf("Matrix before perestanovki:\n");
    print_matrix(M_, N_x - N_y, N_x);
    if (count_stolbikov > 0)
    {
        change_stolbiki(M_, N_y, N_x, perestanovki, base_start, count_stolbikov);
    }

    printf("Complete change matrix:\n");
    print_matrix(M_, N_x - N_y, N_x);

    fprintf(output, "%d\n", q);
    fprintf(output, "%d ", N_x - N_y);
    fprintf(output, "%d\n", N_x);
    for (int i = 0; i < N_x - N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            fprintf(output, "%d ", M_[i][j]);
        }
        fprintf(output, "\n");
    }

    fclose(input);
    fclose(output);
    free_matrix(M, N_y);
    free_matrix(M_, N_x - N_y);

    return 0;
}