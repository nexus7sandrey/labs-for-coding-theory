#pragma warning(disable : 4996)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_matrix(int** M, int N_y, int N_x)
{
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            printf("%d ", M[i][j]);
        }
        printf("\n");
    }
}

int** create_massive(int N_y, int N_x)
{
    int** M = (int**)calloc(N_y, sizeof(int*));
    for (int i = 0; i < N_y; i++)
    {
        M[i] = (int*)calloc(N_x, sizeof(int));
    }
    return M;
}

void free_matrix(int** M, int N_y)
{
    for (int i = 0; i < N_y; i++)
    {
        free(M[i]);
    }
    free(M);
}

//��������� ������������ ��� (��������� ��������� �������)
int check_kind(int** M, int N_y, int N_x)
{
    int* check = (int*)calloc(N_x, sizeof(int)); //���� ������� ������� �������� � �������� ��������� ������
    memset(check, -1, N_x * sizeof(int));
    for (int i = 0; i < N_x; i++)
    {
        int count_0 = 0;
        int count_1 = 0;
        int last_pos_1 = -1;
        for (int j = 0; j < N_y; j++)
        {
            if (M[j][i] == 0)
            {
                count_0++;
            }
            if (M[j][i] == 1)
            {
                count_1++;
                last_pos_1 = j;
            }
        }
        if ((count_0 == N_y - 1) && (count_1 == 1))
        {
            check[i] = last_pos_1;
        }
    }

    //printf("last_pos_1: \n");
    //for (int i = 0; i < N_x; i++)
    //{
    //    printf("%d ", check[i]);
    //}
    //printf("\n");

    int kind_type = 0;
    if (kind_type == 0)
    {
        kind_type = -1; //��������� ������� �����
        for (int i = 0; i < N_y; i++)
        {
            if (check[i] != i)
            {
                kind_type = 0;
                break;
            }
        }
    }
    if (kind_type == 0)
    {
        kind_type = 1; //��������� ������� ������
        for (int i = N_x; i > N_x - N_y; i--)
        {
            if (check[i] != i - (N_x - N_y))
            {
                break;
            }
        }
    }

    return kind_type;
}

//���������, ����� ��� ����� ���� �� ������ q
int check_modul(int** M, int N_y, int N_x, int q)
{
    int check = 0;
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            if (M[i][j] < 0)
            {
                while (M[i][j] < 0)
                {
                    M[i][j] += q;
                }
                check++;
            }
            else if (M[i][j] >= q)
            {
                M[i][j] = M[i][j] % q;
                check++;
            }
        }
    }
    return check;
}

void change_matrix(int** M, int** M_, int N_y, int N_x, int q, int kind)
{
    if (kind < 0) //��������� ������� �����
    {
        for (int i = 0; i < N_y; i++) //������ ������� A
        {
            for (int j = N_y; j < N_x; j++)
            {
                M_[j - N_y][i] = -1 * M[i][j];
            }
        }
        for (int i = N_y; i < N_x; i++) //������ �������� ��� E
        {
            M_[i - N_y][i] = 1;
        }
    }
    else
    {
        for (int i = 0; i < N_y; i++)
        {
            for (int j = 0; j < N_x - N_y; j++)
            {
                M_[j][i + N_x - N_y] = -1 * M[i][j];
            }
        }
        for (int i = 0; i < N_x - N_y; i++)
        {
            M_[i][i] = 1;
        }
    }
    printf("Transporting...\n");
    print_matrix(M_, N_x - N_y, N_x);
    check_modul(M_, N_x - N_y, N_x, q);
}

int open_files(FILE** input, FILE** output, char* argv_0)
{
    char* path_program = (char*)calloc(strlen(argv_0), sizeof(char));
    path_program = argv_0;
    for (int i = strlen(path_program); i > 0; i--)
    {
        if (path_program[i] == '\\')
        {
            path_program[i + 1] = '\0';
            break;
        }
    }

    char* input_file = (char*)calloc(strlen(path_program) + 9, sizeof(char));
    input_file = strcpy(input_file, path_program);
    input_file = strcat(input_file, "input.txt");

    char* output_file = (char*)calloc(strlen(path_program) + 10, sizeof(char));
    output_file = strcpy(output_file, path_program);
    output_file = strcat(output_file, "output.txt");

    (*input) = fopen(input_file, "r");
    (*output) = fopen(output_file, "w");
    int error = 0;
    if (input == NULL)
    {
        printf("Error in open input file!\n");
        error--;
    }
    if (output == NULL)
    {
        printf("Error in open output file!\n");
        error--;
    }

    //free(input_file);
    //free(output_file);
    return error;
}

int main(int argc, char* argv[])
{
    FILE* input = NULL;
    FILE* output = NULL;
    if (open_files(&input, &output, argv[0]) < 0)
    {
        return 0;
    }

    char type;
    int q;
    int N_x;
    int N_y;

    fscanf(input, "%c", &type);
    fscanf(input, "%d", &q);
    fscanf(input, "%d", &N_y);
    fscanf(input, "%d", &N_x);

    printf("q = %d; N_y = %d; N_x = %d\n", q, N_y, N_x);

    if (type == 'G')
    {
        printf("Type G\n");
    }
    if (type == 'H')
    {
        printf("Type H\n");
    }

    char buffer;
    int** M = create_massive(N_y, N_x);
    for (int i = 0; i < N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            fscanf(input, "%d", &M[i][j]);
        }
    }

    printf("Scanned matrix (N_y = %d, N_x = %d): \n", N_y, N_x);
    print_matrix(M, N_y, N_x);

    if (check_modul(M, N_y, N_x, q) != 0)
    {
        printf("Matrix was edited:\n");
        print_matrix(M, N_y, N_x);
    }

    int kind = check_kind(M, N_y, N_x);
    if (kind < 0)
    {
        printf("left E\n");
    }
    else if (kind > 0)
    {
        printf("right E\n");
    }
    else
    {
        printf("Not kanonical view!\n");
    }

    int** M_ = create_massive(N_x - N_y, N_x);
    for (int i = 0; i < N_x - N_y; i++)
    {
        memset(M_[i], 0, N_x * sizeof(int));
    }
    change_matrix(M, M_, N_y, N_x, q, kind);
    if (type == 'G')
    {
        printf("Type H:\n");
        fprintf(output, "H ");
    } 
    if (type == 'H')
    {
        printf("Type G:\n");
        fprintf(output, "G ");
    }
    print_matrix(M_, N_x - N_y, N_x);
    
    fprintf(output, "%d\n", q);
    fprintf(output, "%d ", N_x - N_y);
    fprintf(output, "%d\n", N_x);
    for (int i = 0; i < N_x - N_y; i++)
    {
        for (int j = 0; j < N_x; j++)
        {
            fprintf(output, "%d ", M_[i][j]);
        }
        fprintf(output, "\n");
    }

    fclose(input);
    fclose(output);
    free_matrix(M, N_y);
    free_matrix(M_, N_x - N_y);

    return 0;
}